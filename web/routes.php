<?php

include 'Route/Router.php';
include 'Models/Root.php';
include 'Models/Request.php';

$router = new Router();

$router->add('/', function() {
    $root = new Root();
});

$router->add('/createRoot', function() {
    $root = new Root();
    $root->addRoot('root');
});

$router->add('/getRoots', function() {
   $root = new Root();
   $root->getRootsMap();
});

$router->add('/addBranch', function() {
    $root = new Root();
    $root->addRoot('branch', Request::get('id'));
    $root->getRootsMap();
});

$router->add('/deleteBranch', function() {
    $root = new Root();
    $root->delete(Request::get('id'));
    $root->getRootsMap();
});

$router->add('/editBranch', function() {
    $root = new Root();
    $root->edit(Request::post('id'), Request::post('name'));
});

$router->add('/clearRoots', function() {
   $root = new Root();
   $root->clearRoots();
});

$router->submit();

<?php


class Router
{
    private $_url = array();
    private $_method = array();

    public function add($url, $method = null)
    {
        $this->_url[] = '/'.trim($url, '/');

        if ($method != null) {
            $this->_method[] = $method;
        }
    }

    public function submit()
    {
        $urlGetParam = isset($_GET['url']) ? '/'.$_GET['url'] : '/';
        foreach ($this->_url as $key => $value)
        {
            if (preg_match("#^$value$#", $urlGetParam))
            {
                if (is_string($this->_method[$key])) {
                    $useMethod = $this->_method[$key];
                    new $useMethod();
                    exit;
                }

                call_user_func($this->_method[$key]);
            }
        }
    }
}

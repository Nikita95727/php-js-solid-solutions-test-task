$(document).ready(function()
{
    fetchRoots();
});

$('#createRoot').click(function ()
{
    $.ajax({
        url: '/createRoot',
        success: function () {
            clear();
            fetchRoots();
        }
    });
});

$('#clearRoots').click(function () {
    $.ajax({
        url: '/clearRoots',
        success: function () {
            clear();
            fetchRoots();
        }
    });
});

function fetchRoots()
{
    $.ajax({
        url: '/getRoots',
        success: function (response) {
            if (response.length > 0) {
                clear();
                printRoots(response);
            }
        }
    });
}

function addBranch(id)
{
    $.ajax({
        url: '/addBranch',
        data: {
            id: id
        },

        success: function () {
            clear();
            fetchRoots();
        }
    });
}

function deleteBranch(id)
{
    let isDelete = confirm('Are you sure, you want to delete?');

    if (isDelete) {
        $.ajax({
            url: '/deleteBranch',
            data: {
                id: id
            },

            success: function () {
                clear();
                fetchRoots();
            }
        });
    }
}

function editBranch(id)
{
    let name = prompt('Enter the name of branch');

    if (name) {
        $.ajax({
            type: 'post',
            url: '/editBranch',
            data: {
                id: id,
                name: name,
            },

            success: function () {
                clear();
                fetchRoots();
            }
        });
    }
}

function clear()
{
    $('#roots').empty();
}

function printRoots(roots)
{
    $('#roots').append(roots);
}

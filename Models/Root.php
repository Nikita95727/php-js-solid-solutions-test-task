<?php

include 'db/dbConnection.php';

class Root
{
    public $roots;
    public $connection;
    public $tree;

    public function __construct()
    {
        $dbConnection = new dbConnection();
        $this->connection = $dbConnection->connection;
        $this->fetchRoots();
    }

    public function addRoot($name, $id = null)
    {
        if ($id) {
            $this->connection->query('insert into rootsandbranches(parent_id, name) values('.$id.', "'.$name.'")');
            exit;
        }

        $this->connection->query('insert into rootsandbranches(name) values("'.$name.'")');
    }

    public function delete($id)
    {
        $this->connection->query('delete from rootsandbranches where id = '.$id);
    }

    public function clearRoots()
    {
        $this->connection->query('truncate rootsandbranches');
    }

    public function edit($id, $name)
    {
        $this->connection->query('update rootsandbranches set name = "'.$name.'" where id = '.$id);
    }

    /**
     *
     */
    public function fetchRoots()
    {
        $result = $this->connection->query('select * from rootsandbranches');

        while ($row = mysqli_fetch_array($result)) {
            $this->roots[$row['id']] = $row;
        }
    }

    /**
     * @return mixed
     */
    public function makeRootsMap()
    {
        $children = array();

        if ($this->roots) {
            foreach ($this->roots as $root) {
                $children[$root['id']] = $root['parent_id'];
            }

            foreach ($this->roots as $root) {
                if (!$root['parent_id']) {
                    $this->buildRoot($root, $children);
                }
            }
        }

        return $this->tree;
    }

    /**
     * @param $root
     * @param $children
     */
    public function buildRoot($root, $children)
    {
        $this->_fillRoot($root);

        $ul = false;
        while (true) {
            $key = array_search($root['id'], $children);
            if (!$key) {
                if ($ul) {
                    $this->_addToTree('</ul>');
                }
                break;
            }
            unset($children[$key]);
            if (!$ul) {
                $this->_addToTree('<ul>');
                $ul = true;
            }

            $this->buildRoot($this->roots[$key], $children);
        }

        $this->_addToTree('</span></li>');
    }

    public function getRootsMap()
    {
        echo $this->makeRootsMap();
        exit;
    }

    /**
     * @param $root
     */
    private function _fillRoot($root)
    {
        $this->_addToTree('<li><span>');
        $this->_addToTree($root['name']);
        $this->_addToTree('<button type = "button" id="'.$root['id'].'" onclick="addBranch(this.id)" title="Add branch"><i class="fas fa-plus"></i></button>');
        $this->_addToTree('<button type = "button" id="'.$root['id'].'" onclick="deleteBranch(this.id)" title="Delete branch"><i class="fas fa-minus"></i></button>');
        $this->_addToTree('<button type = "button" id="'.$root['id'].'" onclick="editBranch(this.id)" title="Edit branch"><i class="fas fa-edit"></i></button>');
    }

    /**
     * @param $root
     */
    private function _addToTree($root)
    {
        $this->tree = $this->tree . $root;
    }
}
